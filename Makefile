
.PHONY: all get-bottle serve

all: get-bottle serve


get-bottle:
	test -f bottle.py || curl https://raw.githubusercontent.com/bottlepy/bottle/master/bottle.py -o bottle.py

serve:
	python3 data-source.py


run-grafana:
	docker run -d --network host -e "GF_INSTALL_PLUGINS=grafana-simple-json-datasource" --name=grafana -p 3000:3000 grafana/grafana
